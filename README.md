# README #

This project is a simple Automatic registration module with Selenium python. It imports emails and names required for the registration and run in a loop of emails.

### How do I get set up? ###

You should install python 3.6.4 for Windows.

* Install required python packages
```bash
pip install selenium

pip install xlrd

pip install openpyxl

pip install pandas

pip install DateTime

pip install mysql-connector

```

* Install Chrome web drvier
Copy the chromedriver directory and past to C:

### Contribution guidelines ###

* emails.txt : File name must be fixed
* link.txt : File name must be fixed
* names.txt : File name must be fixed

### Run the script ###

```bash
python app.py
```


### License


This project is licensed under the MIT License
