from flask import Flask, request, jsonify,render_template
from nltk.corpus.reader import lin
import requests
from jinja2 import Template
from datetime import date, timedelta, datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException, UnexpectedAlertPresentException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import time
import shutil
import os
from nltk.corpus import words
import random
from random import sample

"""import .txt file of emails
"""
email_file = open("emails.txt", 'r') 
email_content = email_file.read()
emails = email_content.split('\n')

"""import .txt file of names
"""
name_file = open("names.txt", 'r') 
name_content = name_file.read()
names = name_content.split('\n')

"""import .txt file of a link for registration
"""
link_file = open("link.txt", 'r') 
link_content = link_file.read()
link = link_content

############ Registration with the loop of emails ###############

## A loop with emails

i=0
for email in (emails) :
    # Configure Chrome Web Driver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    chrome_options.add_experimental_option("prefs",prefs)
    driver = webdriver.Chrome("C:\chromedriver\chromedriver.exe", chrome_options=chrome_options)
    driver.set_page_load_timeout("10")
    driver.set_window_size(1341,810)

    print("[" + str(datetime.now()) + "]" + "-INFO" + ": Chrome Web Driver is ready.")

    try:
        driver.get(link)
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'email')))
        print ("[" + str(datetime.now()) + "]" + "-INFO" + ": Page is ready!")

    except TimeoutException:
        try:
            driver.refresh()
            print ("[" + str(datetime.now()) + "]" + "-INFO" + ": Loading took too much time! Retrying...")
            WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'email')))
            print ("[" + str(datetime.now()) + "]" + "-INFO" + ": Page is ready!")

        except TimeoutException:
            print ("[" + str(datetime.now()) + "]" + "-INFO" + ": Loading took too much time!")

    ## Register with email and name
    try:
        print("[" + str(datetime.now()) + "]" + "-INFO" + ": name - " + names[i] + " , email - " + email)
        time.sleep(1)
        driver.find_element_by_name("name").send_keys(names[i])
        time.sleep(1)
        driver.find_element_by_name("email").send_keys(email)
        time.sleep(2)
        driver.find_element_by_name("submit").click()
        time.sleep(2)
    
    except Exception as e:
        print(e)

    print("[" + str(datetime.now()) + "]" + "-INFO" + ": Succeed to register !")

    ## Download 3 pack
    print ("[" + str(datetime.now()) + "]" + "-INFO" + ": Downloading 3 months pack..")
    time.sleep(2)
    try:
        print(driver.find_element_by_xpath(".//button[@class='swal2-deny swal2-styled']"))
        driver.find_element_by_xpath(".//button[@class='swal2-deny swal2-styled']").click()
    except Exception as e:
        print(e)
    time.sleep(10)

    i +=1
    driver.close()